package org.shtarev.tmse02.service;

import org.apache.commons.codec.digest.DigestUtils;
import org.shtarev.tmse02.entyty.User;
import org.shtarev.tmse02.repository.UserRepository;
import org.shtarev.tmse02.Сommands.UserRole;

import java.util.ArrayList;
import java.util.List;

public class UserService {

    private final UserRepository userRepository;

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public void create(final String name, final String password, final UserRole[] userRole) {
        User thisUser = new User();
        thisUser.setName(name);
        thisUser.setPassword(DigestUtils.md5Hex(password));
        thisUser.setUserRole(userRole);
        userRepository.create(thisUser);
    }

    public List<User> logIn() {
        List<User> userCollect = userRepository.logIn();
        return userCollect;
            }

    public List<String> userListS() {
        List<User> userCollect = userRepository.logIn();
        List<String> userNameList = new ArrayList<>();
        for (int i = 0; i < userCollect.size(); i++) {
            User thisUser = userCollect.get(i);
            userNameList.add(i, thisUser.getName());
        }
        return userNameList;
    }

    public List<String> userListID() {
        List<User> userCollect = userRepository.logIn();
        List<String> userIDList = new ArrayList<>();
        for (int i = 0; i < userCollect.size(); i++) {
            User thisUser = userCollect.get(i);
            userIDList.add(i, thisUser.getUserID());
        }
        return userIDList;
    }


    public void rePassword(String name, String oldPassword, String newPassword) {
        List<User> userCollect = userRepository.logIn();
        for (int i = 0; i < userCollect.size(); i++) {
            User thisUser = userCollect.get(i);
            String ooldPassword = DigestUtils.md5Hex(oldPassword);
            if (thisUser.getName().equals(name) || thisUser.getPassword().equals(ooldPassword)) {
                userRepository.rePassword(thisUser.getUserID(), newPassword);
            }
        }
    }

    public void userUpdate(String name, String thisUserId) {
        userRepository.userUpdate(name, thisUserId);
    }
}

