package org.shtarev.tmse02.service;

import org.shtarev.tmse02.entyty.Project;
import org.shtarev.tmse02.repository.ProjectRepository;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.ResolverStyle;
import java.util.List;

public class ProjectService {
    private final ProjectRepository projectRepository;
    private static final DateTimeFormatter dateTimeFormatter = DateTimeFormatter
            .ofPattern("dd-MM-uuuu")
            .withResolverStyle(ResolverStyle.STRICT);

    public ProjectService(final ProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    public void create(final String name, final String description, final String startDate, final String finishDate, final String thisUserId) {
        if (name.isEmpty() || description.isEmpty() || startDate.isEmpty() || finishDate.isEmpty()) return;
        if (name == null || description == null || startDate == null || finishDate == null) return;
        final LocalDate startDateTR = LocalDate.parse(startDate, dateTimeFormatter);
        final LocalDate finishDateTR = LocalDate.parse(finishDate, dateTimeFormatter);
        final Project thisProject = new Project();
        thisProject.setName(name);
        thisProject.setDescription(description);
        thisProject.setDataStart(startDateTR);
        thisProject.setDataFinish(finishDateTR);
        thisProject.setUserId(thisUserId);
        projectRepository.persist(thisProject);
    }

    public Project read(final String id) {
        if (id.isEmpty() || id == null) return null;
        return projectRepository.findOne(id);
    }

    public void update(final String projectId, final String name, final String description, final String startDate, final String finishDate) {
        if (projectId.isEmpty() || name.isEmpty() || description.isEmpty() || startDate.isEmpty() || finishDate.isEmpty())
            return;
        if (projectId == null || name == null || description == null || startDate == null || finishDate == null) return;
        final LocalDate startDateP = LocalDate.parse(startDate, dateTimeFormatter);
        final LocalDate finishDateP = LocalDate.parse(finishDate, dateTimeFormatter);
        projectRepository.merge(projectId, name, description, startDateP, finishDateP);
    }

    public void delete(final String id, final String userID) {
        if (id.isEmpty() || id == null) return;
        projectRepository.remove(id, userID);
    }

    public List<Project> readAllProjects(final String userID) {
        List<Project> projectList = projectRepository.findAll(userID);
        return projectList;
    }

    public void deleteAllProject(final String userID) {
        projectRepository.removeAll(userID);
    }
}

