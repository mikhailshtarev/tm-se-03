package org.shtarev.tmse02.service;

import org.shtarev.tmse02.entyty.Task;
import org.shtarev.tmse02.repository.ProjectRepository;
import org.shtarev.tmse02.repository.TaskRepository;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.ResolverStyle;
import java.util.List;
import java.util.stream.Collectors;

public class TaskService {
    private final ProjectRepository projectRepository;
    private final TaskRepository taskRepository;
    private static final DateTimeFormatter dateTimeFormatter = DateTimeFormatter
            .ofPattern("dd-MM-uuuu")
            .withResolverStyle(ResolverStyle.STRICT);

    public TaskService(ProjectRepository projectRepository, TaskRepository taskRepository) {
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    public void create(final String name, final String description, final String dataStart,
                       final String dataFinish, final String projectID, final String thisUserID) {
        if (name == "") return;
        if (description == "") return;
        if (dataStart == "") return;
        if (dataFinish == "") return;
        final LocalDate dataStartTR = LocalDate.parse(dataStart, dateTimeFormatter);
        final LocalDate dataFinishTR = LocalDate.parse(dataFinish, dateTimeFormatter);
        final Task thisTask = new Task();
        thisTask.setName(name);
        thisTask.setDescription(description);
        thisTask.setDataStart(dataStartTR);
        thisTask.setDataFinish(dataFinishTR);
        thisTask.setProjectId(projectID);
        thisTask.setUserID(thisUserID);
        taskRepository.persist(thisTask);
    }

    public Task read(final String id) {
        if (id == "") return null;
        else
            return taskRepository.findOne(id);
    }

    public void update(final String id, final String name, final String description,
                       final String dataStart, final String dataFinish, final String projectId) {
        if (name.isEmpty() || description.isEmpty() || dataStart.isEmpty() || dataFinish.isEmpty() || id.isEmpty())
            return;
        if (name == null || description == null || dataStart == null || dataFinish == null || id == null) return;
        final LocalDate dataStartT = LocalDate.parse(dataStart, dateTimeFormatter);
        final LocalDate dataFinishT = LocalDate.parse(dataFinish, dateTimeFormatter);
        taskRepository.merge(id, name, description, dataStartT, dataFinishT, projectId);
    }

    public void delete(final String id, final String userId) {
        if (id.isEmpty() || id == null) return;
        taskRepository.remove(id, userId);
    }

    public List<Task> readTaskForProject(final String projectID, final String userId) {
        if (projectID.isEmpty() || projectID == null) return null;
        List<Task> taskListById = taskRepository.findAll(userId);
        return taskListById.stream().filter(thisTask -> thisTask.getProjectId().equals(projectID))
                .collect(Collectors.toList());

    }

    public void deleteById(final String idProject, final String userId) {
        if (idProject.isEmpty() || idProject == null) return;
        List<Task> taskListById = taskRepository.findAll(userId);
        for (int i = 0; i < taskListById.size(); i++) {
            Task thisTask = taskListById.get(i);
            if (thisTask.getProjectId().equals(idProject)) {
                taskRepository.remove(thisTask.getId(), userId);
            } else continue;
        }
    }

    public List<Task> readAllTask(final String userId) {
        List<Task> taskList = taskRepository.findAll(userId);
        return taskList;
    }

    public void deleteAllTask(final String userId) {
        taskRepository.removeAll(userId);
    }
}





