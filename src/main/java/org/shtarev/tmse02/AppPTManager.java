package org.shtarev.tmse02;

import org.shtarev.tmse02.Сommands.*;

public class AppPTManager {

    static Class[] classes = {HelpCommand.class, ProjectListCommand.class, ProjectCreateCommand.class,
            ProjectClearCommands.class, ProjectRemoveCommand.class, TaskClearCommand.class, TaskCreateCommand.class,
            TaskListCommand.class, TaskRemoveCommand.class, UserCreate.class, UserLogIn.class, UserList.class,
            UserRePassword.class, UserLogOut.class, UserUpdate.class};

    public static void main(String[] args) {
        Bootstrap bootstrap = new Bootstrap();
        bootstrap.createTwoUser();
        bootstrap.init(classes);
        bootstrap.start();

    }
}
