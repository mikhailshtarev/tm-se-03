package org.shtarev.tmse02.entyty;

import org.shtarev.tmse02.Сommands.UserRole;

import java.util.UUID;

public class User {
    private String name;
    private String password;
    private UserRole[] userRole;
    private final String userID = UUID.randomUUID().toString();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public UserRole[] getUserRole() {
        return userRole;
    }

    public void setUserRole(UserRole[] userRole) {
        this.userRole = userRole;
    }

    public String getUserID() {
        return userID;
    }
}
