package org.shtarev.tmse02.entyty;

import java.time.LocalDate;
import java.util.UUID;

public class Task {

    private final String id= UUID.randomUUID().toString();
    private String name;
    private String description;
    private LocalDate dataStart;
    private LocalDate dataFinish;
    private String projectId;
    private String UserID;

    public String getId() {return id;}

    public String getName() { return name;}

    public void setName(String name) {this.name = name;}

    public String getDescription() {return description;}

    public void setDescription(String description) {this.description = description;}

    public LocalDate getDataStart() {return dataStart;}

    public void setDataStart(LocalDate dataStart) {this.dataStart = dataStart;}

    public LocalDate getDataFinish() {return dataFinish;}

    public void setDataFinish(LocalDate dataFinish) {this.dataFinish = dataFinish;}

    public String getProjectId() {return projectId;}

    public void setProjectId(String projectId) {this.projectId = projectId;}

    public String getUserID() {
        return UserID;
    }

    public void setUserID(String userID) {
        UserID = userID;
    }
}