package org.shtarev.tmse02.entyty;

import java.time.LocalDate;
import java.util.UUID;

public class Project {
    private final  String id= UUID.randomUUID().toString();
    private String name;
    private String description;
    private LocalDate dataStart ;
    private LocalDate dataFinish ;
    private String userId;

    public void setName(String name) { this.name = name;}
    public void setDescription(String description) { this.description = description;}
    public void setDataStart(LocalDate dataStart) { this.dataStart = dataStart;}
    public void setDataFinish(LocalDate dataFinish) { this.dataFinish = dataFinish;}
    public String getId() {return id;}
    public String getName() {
        return name;
    }
    public String getDescription() {
        return description;
    }
    public LocalDate getDataStart() {
        return dataStart;
    }
    public LocalDate getDataFinish() {
        return dataFinish;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
