package org.shtarev.tmse02.Сommands;

final public class TaskRemoveCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "TaskRemove";
    }

    @Override
    public String getDescription() {
        return "Remove selected Task";
    }

    @Override
    public UserRole[] getListRole() {
        UserRole[] userRole = {UserRole.ADMIN,UserRole.REGULAR_USER};
        return  userRole;
    }

    @Override
    public void execute() throws Exception {
        System.out.println("Введите ID задачи которую хотите удалить:");
        String taskId = bootstrap.terminalService.nextLine();
        bootstrap.getTaskService().delete(taskId,bootstrap.bootUser.getUserID());
    }
}
