package org.shtarev.tmse02.Сommands;

import org.shtarev.tmse02.Bootstrap;

public abstract class AbstractCommand {

    protected Bootstrap bootstrap;

    public void setBootstrap(Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    public abstract String getName();

    public abstract String getDescription();

    public abstract UserRole[] getListRole();

    public abstract void execute() throws Exception;
}
