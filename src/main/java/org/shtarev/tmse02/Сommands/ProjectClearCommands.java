package org.shtarev.tmse02.Сommands;


public final class ProjectClearCommands extends AbstractCommand {
    @Override
    public String getName() {
        return "ProjectClear";
    }

    @Override
    public String getDescription() {
        return "Delete all Project with her tasks";
    }

    @Override
    public UserRole[] getListRole() {
        UserRole[] userRole = {UserRole.ADMIN};
        return  userRole;
    }

    @Override
    public void execute() {
        bootstrap.getProjectService().deleteAllProject(bootstrap.bootUser.getUserID());
        bootstrap.getTaskService().deleteAllTask(bootstrap.bootUser.getUserID());
    }
}
