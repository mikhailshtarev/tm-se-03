package org.shtarev.tmse02.Сommands;

final public class TaskClearCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "TaskClear";
    }

    @Override
    public String getDescription() {
        return "Remove all Tasks";
    }

    @Override
    public UserRole[] getListRole() {
        UserRole[] userRole = {UserRole.ADMIN};
        return  userRole;
    }

    @Override
    public void execute() throws Exception {
        bootstrap.getTaskService().deleteAllTask(bootstrap.bootUser.getUserID());
    }
}
