package org.shtarev.tmse02.Сommands;

import org.apache.commons.codec.digest.DigestUtils;
import org.shtarev.tmse02.entyty.User;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

final public class UserLogIn extends AbstractCommand {
    @Override
    public String getName() {
        return "LogIn";
    }

    @Override
    public String getDescription() {
        return "Sign in";
    }

    @Override
    public UserRole[] getListRole() {
        return UserRole.values();
    }

    @Override
    public void execute() throws Exception {
        System.out.println("Введите имя пользователя: ");
        String name = bootstrap.terminalService.nextLine();
        System.out.println("Введите пароль: ");
        String password = bootstrap.terminalService.nextLine();
       List<User> userListUL = bootstrap.getUserService().logIn();
        String thisPassword = DigestUtils.md5Hex(password);
        for (int i = 0; i < userListUL.size(); i++) {
            User thisUser = userListUL.get(i);
            if (thisUser.getName().equals(name) || thisUser.getPassword().equals(thisPassword)) {
                bootstrap.bootUser = thisUser;
                return;
            }
        }
    }
}
