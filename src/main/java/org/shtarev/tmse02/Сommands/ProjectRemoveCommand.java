package org.shtarev.tmse02.Сommands;

final public class ProjectRemoveCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "ProjectRemove";
    }

    @Override
    public String getDescription() {
        return "Remove selected project by its ID";
    }

    @Override
    public UserRole[] getListRole() {
        UserRole[] userRole = {UserRole.ADMIN,UserRole.REGULAR_USER};
        return  userRole;
    }

    @Override
    public void execute() throws Exception {
        System.out.println("Введите ID проекта который хотите удалить:");
        String projectID = bootstrap.terminalService.nextLine();
        bootstrap.getProjectService().delete(projectID,bootstrap.bootUser.getUserID());
        bootstrap.getTaskService().deleteById(projectID,bootstrap.bootUser.getUserID());
    }
}
