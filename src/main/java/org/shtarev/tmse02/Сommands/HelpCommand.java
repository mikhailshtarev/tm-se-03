package org.shtarev.tmse02.Сommands;

import java.util.List;

public final class HelpCommand extends AbstractCommand {
    @Override
    public String getName() {return "help";}

    @Override
    public String getDescription() {
        return "Show all commands";
    }

    @Override
    public UserRole[] getListRole() {
        return UserRole.values();
    }

    @Override
    public void execute() {
        final List<AbstractCommand> lists=bootstrap.getCommands();
        for (int i=0;i<lists.size();i++) {
            AbstractCommand thisCommand = lists.get(i);
            System.out.println("Название команды: "+ thisCommand.getName()+
                    "  Описание команды: " + thisCommand.getDescription());
        }
    }
}
