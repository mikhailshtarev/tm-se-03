package org.shtarev.tmse02.Сommands;

import org.shtarev.tmse02.entyty.Task;

import java.util.List;

final public class TaskListCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "TaskList";
    }

    @Override
    public String getDescription() {
        return "Show all Tasks";
    }

    @Override
    public UserRole[] getListRole() {
        UserRole[] userRole = {UserRole.ADMIN,UserRole.REGULAR_USER};
        return  userRole;
    }

    @Override
    public void execute() throws Exception {
        List<Task> taskList = bootstrap.getTaskService().readAllTask(bootstrap.bootUser.getUserID());
        for (int i = 0; i <= taskList.size(); i++) {
            Task thisTask = taskList.get(i);
            System.out.println("Название задачи: " + thisTask.getName() + "  ID задачи:  " + thisTask.getId() +
                    "  ID проекта к которому относится задача:  " + thisTask.getProjectId());
        }

    }
}
