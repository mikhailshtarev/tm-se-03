package org.shtarev.tmse02.Сommands;

import java.util.List;

final public class UserList extends AbstractCommand {
    @Override
    public String getName() {
        return "UserList";
    }

    @Override
    public String getDescription() {
        return "View all Users";
    }

    @Override
    public UserRole[] getListRole() {
        UserRole[] userRole = {UserRole.ADMIN,UserRole.REGULAR_USER};
        return  userRole;
    }

    @Override
    public void execute() throws Exception {
        List<String> userNameList=bootstrap.getUserService().userListS();
        List<String> userIDList=bootstrap.getUserService().userListID();
        for (int i=0; i<userNameList.size();i++) {
        System.out.println("Имя пользователя: " + userNameList.get(i) + "  ID пользователя: " + userIDList.get(i));
        }
    }
}
