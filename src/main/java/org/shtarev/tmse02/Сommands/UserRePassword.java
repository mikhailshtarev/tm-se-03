package org.shtarev.tmse02.Сommands;

final public class UserRePassword extends AbstractCommand {
    @Override
    public String getName() {
        return "RePassword";
    }

    @Override
    public String getDescription() {
        return "Update you password";
    }

    @Override
    public UserRole[] getListRole() {
        UserRole[] userRole = {UserRole.ADMIN,UserRole.REGULAR_USER};
        return  userRole;
    }

    @Override
    public void execute() throws Exception {
        System.out.println("Введите имя пользователя, у которого хотите поменять пароль: ");
        String name = bootstrap.terminalService.nextLine();
        System.out.println("Введите старый пароль: ");
        String oldPassword = bootstrap.terminalService.nextLine();
        System.out.println("Введите новый пароль: ");
        String newPassword = bootstrap.terminalService.nextLine();
        bootstrap.getUserService().rePassword(name, oldPassword,newPassword);

    }
}
