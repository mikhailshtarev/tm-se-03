package org.shtarev.tmse02.Сommands;

final public class TaskCreateCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "TaskCreate";
    }

    @Override
    public String getDescription() {
        return "Create new Task";
    }

    @Override
    public UserRole[] getListRole() {
        UserRole[] userRole = {UserRole.ADMIN,UserRole.REGULAR_USER};
        return  userRole;
    }

    @Override
    public void execute() throws Exception {
        System.out.println("Введите название задачи:");
        String name = bootstrap.terminalService.nextLine();
        System.out.println("Введите описание задачи:");
        String description = bootstrap.terminalService.nextLine();
        System.out.println("Введите дату начала задачи: ");
        String dataStart = bootstrap.terminalService.nextLine();
        System.out.println("Введите дату окончания задачи: ");
        String dataFinish = bootstrap.terminalService.nextLine();
        System.out.println("Введите ID проекта, к которому относится задача: ");
        String projectID = bootstrap.terminalService.nextLine();
        String thisUserID = bootstrap.bootUser.getUserID();
        bootstrap.getTaskService().create(name, description, dataStart, dataFinish, projectID,thisUserID);

    }
}
