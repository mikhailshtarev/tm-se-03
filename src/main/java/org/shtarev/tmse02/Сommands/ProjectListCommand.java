package org.shtarev.tmse02.Сommands;

import org.shtarev.tmse02.entyty.Project;

import java.util.List;

public final class ProjectListCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "ProjectList";
    }

    @Override
    public String getDescription() {
        return "Show all projects.";
    }

    @Override
    public UserRole[] getListRole() {
        UserRole[] userRole = {UserRole.ADMIN,UserRole.REGULAR_USER};
        return  userRole;
    }

    @Override
    public void execute()  {
        System.out.println("[PROJECT LIST]");
        List<Project> projectList = bootstrap.getProjectService().readAllProjects(bootstrap.bootUser.getUserID());
        for (int i=0;i<=projectList.size();i++) {
            Project thisProject = projectList.get(i);
            System.out.println("Название проекта: " + thisProject.getName() + "  ID проекта: " + thisProject.getId()+ "  ID Usera: " + thisProject.getUserId() );
        }


    }
}
