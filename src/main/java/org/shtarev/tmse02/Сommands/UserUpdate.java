package org.shtarev.tmse02.Сommands;

import org.shtarev.tmse02.entyty.User;

final public class UserUpdate extends AbstractCommand {
    @Override
    public String getName() {
        return "UserUpdate";
    }

    @Override
    public String getDescription() {
        return "Update user profile";
    }

    @Override
    public UserRole[] getListRole() {
        UserRole[] userRole = {UserRole.ADMIN,UserRole.REGULAR_USER};
        return  userRole;
    }

    @Override
    public void execute() throws Exception {
        System.out.println("Введите новое имя  пользователя:");
        String name = bootstrap.terminalService.nextLine();
        User thisUser = bootstrap.bootUser;
        String thisUserId=thisUser.getUserID();
        bootstrap.getUserService().userUpdate(name,thisUserId);
    }
}
