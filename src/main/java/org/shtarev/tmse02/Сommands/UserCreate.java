package org.shtarev.tmse02.Сommands;

import static org.shtarev.tmse02.Сommands.UserRole.REGULAR_USER;

final public class UserCreate extends AbstractCommand {
    @Override
    public String getName() {
        return "CreateUser";
    }

    @Override
    public String getDescription() {
        return "Create New User";
    }

    @Override
    public UserRole[] getListRole() {
    return UserRole.values();
    }

    @Override
    public void execute() throws Exception {
        System.out.println("Введите имя нового пользователя: ");
        String name = bootstrap.terminalService.nextLine();
        System.out.println("Введите пароль для этого пользователя: ");
        String password = bootstrap.terminalService.nextLine();
        UserRole[] userRole = {REGULAR_USER};
        bootstrap.getUserService().create(name,password,userRole);
    }
}
