package org.shtarev.tmse02.Сommands;

import org.shtarev.tmse02.entyty.User;

final public class UserLogOut extends AbstractCommand {
    @Override
    public String getName() {
        return "UserLogout";
    }

    @Override
    public String getDescription() {
        return "User ends the session";
    }

    @Override
    public UserRole[] getListRole() {
        UserRole[] userRole = {UserRole.ADMIN,UserRole.REGULAR_USER};
        return  userRole;
    }

    @Override
    public void execute() throws Exception {
        bootstrap.bootUser= new User();
        return;
    }
}
