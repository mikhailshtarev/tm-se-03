package org.shtarev.tmse02.Сommands;


public final class ProjectCreateCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "ProjectCreate";
    }

    @Override
    public String getDescription() {
        return "Create a new Project";
    }

    @Override
    public UserRole[] getListRole() {
        UserRole[] userRole = {UserRole.ADMIN,UserRole.REGULAR_USER};
        return  userRole;
    }

    @Override
    public void execute() {
        System.out.println("Введите название проекта,который хотите создать: ");
        String projectName = bootstrap.terminalService.nextLine();
        System.out.println("Введите описание проекта: ");
        String description = bootstrap.terminalService.nextLine();
        System.out.println("Введите дату начала проекта: ");
        String dateStartSP = bootstrap.terminalService.nextLine();
        System.out.println("Введите дату окончания проекта: ");
        String dateFinishSP = bootstrap.terminalService.nextLine();
        bootstrap.getProjectService().create(projectName, description, dateStartSP, dateFinishSP,bootstrap.bootUser.getUserID());

    }
}
