package org.shtarev.tmse02;

import org.shtarev.tmse02.entyty.User;
import org.shtarev.tmse02.repository.ProjectRepository;
import org.shtarev.tmse02.repository.TaskRepository;
import org.shtarev.tmse02.repository.UserRepository;
import org.shtarev.tmse02.service.ProjectService;
import org.shtarev.tmse02.service.TaskService;
import org.shtarev.tmse02.service.UserService;
import org.shtarev.tmse02.Сommands.AbstractCommand;
import org.shtarev.tmse02.Сommands.UserRole;

import java.util.*;
import java.util.stream.Collectors;

import static org.shtarev.tmse02.Сommands.UserRole.ADMIN;
import static org.shtarev.tmse02.Сommands.UserRole.REGULAR_USER;

public final class Bootstrap {
    private final TaskRepository taskRepository = new TaskRepository();
    private final ProjectRepository projectRepository = new ProjectRepository();
    private final UserRepository userRepository = new UserRepository();
    private final UserService userService = new UserService(userRepository);
    private final TaskService taskService = new TaskService(projectRepository, taskRepository);
    private final ProjectService projectService = new ProjectService(projectRepository);
    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();
    public User bootUser = new User();
    public final Scanner terminalService = new Scanner(System.in);
    public ProjectService getProjectService() {
        return projectService;
    }

    public TaskService getTaskService() {
        return taskService;
    }

    public UserService getUserService() {
        return userService;
    }

    final protected void createTwoUser() {
       final UserRole[] userRole = {ADMIN};
        final UserRole[] userRole2 = {REGULAR_USER};
        userService.create("admin", "admin", userRole);
        userService.create("regularUser", "regularUser", userRole2);

    }

    public void init(Class[] classes) {
        try {
            for (Class cl : classes) {
                Object o = cl.newInstance();
                if (o instanceof AbstractCommand) {
                    registry((AbstractCommand) o);
                } else throw new IllegalArgumentException("error");
            }
        } catch (IllegalAccessException | InstantiationException e) {
            e.printStackTrace();
        }
    }

    public void registry(final AbstractCommand command) {
        final String cliCommand = command.getName();
        final String cliDescription = command.getDescription();
        if (cliCommand == null || cliCommand.isEmpty())
            try {
                throw new Exception();
            } catch (Exception e) {
                e.printStackTrace();
            }
        if (cliDescription == null || cliDescription.isEmpty())
            try {
                throw new Exception();
            } catch (Exception e) {
                e.printStackTrace();
            }
        command.setBootstrap(this);
        commands.put(cliCommand, command);
    }

    public List<AbstractCommand> getCommands() {
        return new ArrayList<>(commands.values());
    }

    public void start() {
        try {
            System.out.println("*** WELCOME TO TASK MANAGER ***");
            String command = "";
            while (!"exit".equals(command)) {
                try {
                    try {
                        List<UserRole> listRole = Arrays.stream(bootUser.getUserRole()).collect(Collectors.toList());
                        for (int i = 0; i < listRole.size(); i++) {
                            UserRole thisRole = listRole.get(i);
                            ArrayList<String> listRoleStr = new ArrayList<>();
                            listRoleStr.add(thisRole.toString());
                        }
                        System.out.print("Role: " + listRole.stream().findAny() +
                                " |  Enter the terminal command: ");
                    } catch (Exception e) {
                        UserRole[] x = {UserRole.NO_ROLE};
                        bootUser.setUserRole(x);
                        continue;
                    }
                    command = terminalService.nextLine();
                    execute(command);
                } catch (Exception e) {
                    System.out.println(e.getMessage() + "Error");
                } finally {
                    continue;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void execute(final String command) throws Exception {
        if (command == null || command.isEmpty()) return;
        final AbstractCommand abstractCommand = commands.get(command);
        if (abstractCommand == null) return;
        UserRole[] userRoleList = abstractCommand.getListRole();
        UserRole[] userRoleList2 = bootUser.getUserRole();
        ArrayList<UserRole> arListRole = Arrays.stream(userRoleList).collect(Collectors.toCollection(ArrayList::new));
        ArrayList<UserRole> arListRole2 = Arrays.stream(userRoleList2).collect(Collectors.toCollection(ArrayList::new));
        for (int i=0; i< arListRole.size();i++) {
            UserRole thisRole = arListRole.get(i);
            for (int j=0;j< arListRole2.size();j++){
                UserRole thisRole2 = arListRole2.get(j);
                if (thisRole.equals(thisRole2))
                    abstractCommand.execute();
            }
        }

//        if (Arrays.stream(bootUser.getUserRole()).anyMatch(userRole -> userRole.equals(Arrays.stream(userRoleList).)))
//            abstractCommand.execute();
//
    }
}


