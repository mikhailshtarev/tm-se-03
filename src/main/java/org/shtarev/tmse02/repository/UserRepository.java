package org.shtarev.tmse02.repository;

import org.apache.commons.codec.digest.DigestUtils;
import org.shtarev.tmse02.entyty.User;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class UserRepository {
    private final Map<String, User> userMap = new HashMap<>();


    public void create(final User thisUser) {
        userMap.put(thisUser.getUserID(), thisUser);
    }

    public List<User> logIn() {
        List<User> userAList = new ArrayList<>(userMap.values());
        return userAList;
    }

    public void rePassword(final String userID, final String newPassword) {
        User thisUser = userMap.get(userID);
        String newPassword1 = DigestUtils.md5Hex(newPassword);
        thisUser.setPassword(newPassword1);
    }

    public void userUpdate(String name, String thisUserID) {
        User thisUser = userMap.get(thisUserID);
        thisUser.setName(name);
    }
}
