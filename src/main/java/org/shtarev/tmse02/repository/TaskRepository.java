package org.shtarev.tmse02.repository;

import org.shtarev.tmse02.entyty.Task;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class TaskRepository {
    private final Map<String, Task> taskMap = new HashMap<String, Task>();

    public void persist(final Task thisTask) {
        if (!taskMap.containsValue(thisTask))
            taskMap.put(thisTask.getId(), thisTask);
        else return;
    }

    public Task findOne(final String id) {
        return taskMap.get(id);
    }

    public void merge(final String id, final String name, final String description, final LocalDate dataStart,
                      final LocalDate dataFinish, final String projectId) {
        Task thisTask = new Task();
        thisTask.setName(name);
        thisTask.setDescription(description);
        thisTask.setDataStart(dataStart);
        thisTask.setDataFinish(dataFinish);
        thisTask.setProjectId(projectId);
        if (!taskMap.containsKey(id))
            taskMap.put(id, thisTask);
        else {
            Task thisTaskR = taskMap.get(id);
            thisTaskR.setName(name);
            thisTaskR.setDescription(description);
            thisTaskR.setDataStart(dataStart);
            thisTaskR.setDataFinish(dataFinish);
            thisTaskR.setProjectId(projectId);
        }
    }

    public void remove(final String id, final String userID) {
        Task thisTask = taskMap.get(id);
        if (thisTask.getUserID().equals(userID)) {
            taskMap.remove(id);
        }

    }

        public List<Task> findAll(final String userID) {
        List<Task> taskListValue = new ArrayList<>(taskMap.values());
        return taskListValue.stream().filter(task -> task.getUserID().equals(userID)).collect(Collectors.toList());
    }
//    public List<Task> findAll() {
//        List<Task> taskListValue = new ArrayList<>(taskMap.values());
//        return taskListValue;
//    }

    public void removeAll(final String userID) {
        List<Task> taskListValue = new ArrayList<>(taskMap.values());
        List<Task> taskListValue2 = taskListValue.stream().filter(task -> task.getUserID().equals(userID)).
                collect(Collectors.toList());
        for (int i = 0; i < taskListValue2.size(); i++) {
            Task thisTask = taskListValue2.get(i);
            taskMap.remove(thisTask.getUserID());
        }
    }
}


