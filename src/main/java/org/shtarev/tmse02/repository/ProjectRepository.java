package org.shtarev.tmse02.repository;

import org.shtarev.tmse02.entyty.Project;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;


public class ProjectRepository {
    private final HashMap<String, Project> projectMap = new HashMap<String, Project>();

    public void persist(Project thisProject) {
        if (!projectMap.containsValue(thisProject))
            projectMap.put(thisProject.getId(), thisProject);
        else return;
    }

    public Project findOne(final String id) {
        return projectMap.get(id);
    }

    public void merge(final String projectId, final String name, final String description,
                      final LocalDate startDateP, final LocalDate finishDateP) {
        Project thisProject = new Project();
        thisProject.setName(name);
        thisProject.setDescription(description);
        thisProject.setDataStart(startDateP);
        thisProject.setDataFinish(finishDateP);
        if (!projectMap.containsKey(projectId))
            projectMap.put(projectId, thisProject);
        else {
            Project thisProjectR = projectMap.get(projectId);
            thisProjectR.setName(name);
            thisProjectR.setDescription(description);
            thisProjectR.setDataStart(startDateP);
            thisProjectR.setDataFinish(finishDateP);
        }
    }

    public void remove(final String Id, final String userID) {
        Project thisProject = projectMap.get(Id);
        if (thisProject.getUserId().equals(userID)) {
            projectMap.remove(Id);
        }
    }

    public List<Project> findAll(final String userID) {
        List<Project> projectListValue = new ArrayList<>(projectMap.values());
        return projectListValue.stream().filter(project -> project.getUserId().equals(userID)).collect(Collectors.toList());
    }

    public void removeAll(final String userID) {
        List<Project> projectListValue = new ArrayList<>(projectMap.values());
        List<Project> project2List = projectListValue.stream().filter(project -> project.getUserId().equals(userID))
                .collect(Collectors.toList());
        for (int i = 0; i < project2List.size(); i++) {
            Project thisProject = project2List.get(i);
            projectMap.remove(thisProject.getId());
        }
    }
}

